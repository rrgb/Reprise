require "gtk3"
require "sequel"
require "nokogiri"
require "open-uri"
require "uri"
require "public_suffix"
require "launchy"
require "fileutils"

GLib.application_name = 'reprise'

@cd = "#{GLib.user_config_dir}/#{GLib.application_name}"
@dd = "#{GLib.user_data_dir}/#{GLib.application_name}"
FileUtils.mkdir_p(@cd)
FileUtils.mkdir_p(@dd)

puts @cd
puts @dd

@keyfile = GLib::KeyFile.new
@keyfilename = "#{GLib.application_name}.ini"

begin
  puts "Loading keyfile."
  result = @keyfile.load_from_file("#{@cd}/#{@keyfilename}")
rescue GLib::FileError => e
  puts "Not Found. Creating keyfile."
  File.open("#{@cd}/#{@keyfilename}", 'w') do |out|
    out.write @keyfile.to_data
  end
end

# Database

DB = Sequel.sqlite("#{@dd}/data.db")
Sequel::Model.plugin :timestamps
DB.create_table? :items do
  primary_key :id
  String :title
  String :url, :unique => true
  timestamp :timestamp
end

@items = DB[:items]

glade_file = "#{File.expand_path(File.dirname(__FILE__))}/window_main.glade"
builder = Gtk::Builder.new
builder.add_from_file(glade_file)

@window_main = builder.get_object("window_main")
@textentry_main = builder.get_object("textentry_main")
@scrolled_main = builder.get_object("scrolled_main")
treeview_main = builder.get_object("treeview_main")
@revealer = builder.get_object("revealer")
search_button = builder.get_object("button")

@status_revealer = builder.get_object("status_revealer")
status_bar = builder.get_object("status_bar")
status_close = builder.get_object("status_close")

@status_revealer.set_transition_duration(500)
@status_revealer.set_transition_type(Gtk::Revealer::TransitionType::SLIDE_UP)

context_id = status_bar.get_context_id("alertbar")
status_bar.push(context_id, "Duplicate not added.")
#statusbar.show

@cb = Gtk::Clipboard.get(Gdk::Selection::CLIPBOARD)
status_close.signal_connect_after("clicked") {
  @status_revealer.set_reveal_child(false)
}
#

# Tree view

#provider = Gtk::CssProvider.new
#provider.load(:data => <<-CSS)
  #.view row:nth-child(even) { background-color: #FF0000; }

#CSS
#Gtk::StyleContext.add_provider_for_screen(Gdk::Display.default.default_screen, provider, Gtk::StyleProvider::PRIORITY_APPLICATION)

#con = treeview_main.style_context
#GtkTreeView row:nth-child(even) { background-color: #FF0000; }
#GtkTreeView row:nth-child(odd) { background-color: #00FF00; }
#treeview_main.rules_hint=true


@model = Gtk::TreeStore.new(String, String, Integer, String)
#@model_filter = Gtk::TreeModelFilter.new(@model)
render = Gtk::CellRendererText.new
col_1 = Gtk::TreeViewColumn.new("Title", render, {:markup => 0})
col_2 = Gtk::TreeViewColumn.new("URL")
col_3 = Gtk::TreeViewColumn.new("ID")
col_4 = Gtk::TreeViewColumn.new("PlainTitle")

tree_menu = Gtk::Menu.new
tree_menuitem_copy_url = Gtk::MenuItem.new(:label => "Copy Url")
tree_menuitem_copy_url.signal_connect("activate") {
  puts "copy url"
  @cb.set_text(@p_iter.get_value(1))
}
tree_menuitem_copy_name = Gtk::MenuItem.new(:label => "Copy Name")
tree_menuitem_copy_name.signal_connect("activate") {
  puts "copy name"
  doc = Nokogiri::HTML(@p_iter.get_value(0))
  @cb.set_text(doc.at('span').text.strip)
}
tree_menuitem_remove = Gtk::MenuItem.new(:label => "Remove")
tree_menuitem_remove.signal_connect("activate") {
  puts "remove"
  @items.where(:id => @p_iter.get_value(2)).delete()
  @model.remove(@p_iter)
}
tree_menu.append(tree_menuitem_copy_url)
tree_menu.append(tree_menuitem_copy_name)
tree_menu.append(tree_menuitem_remove)

treeview_main.append_column(col_1)
treeview_main.append_column(col_2)
treeview_main.append_column(col_3)
treeview_main.append_column(col_4)

treeview_main.set_model(@model)
treeview_main.set_enable_search(true)
treeview_main.set_search_column(0)
treeview_main.set_search_equal_func do |model, column, key, iter, data|
  #puts model.get_value(iter, column)
  puts key
  puts key.class
  #model.clear

  if model.get_value(iter, column).upcase.include? key.upcase
    puts "included"
    puts model.get_value(iter, 3)
    #itr = model.prepend(nil)
    #itr.set_value(0, model.get_value(iter, 0))
    #itr.set_value(1, model.get_value(iter, 1))
    #itr.set_value(2, model.get_value(iter, 2))
    #itr.set_value(3, model.get_value(iter, 3))
    false
  else
    true
  end
end



treeview_main.signal_connect("button-press-event") do |treeview, event|
  if event.button == 3
    model = treeview.model

    result_arr = treeview.get_path_at_pos(event.x, event.y)
    puts "res arr #{result_arr}"
    @p_iter = model.get_iter(result_arr[0])

    tree_menu.show_all
    tree_menu.popup(nil, nil, event.button, event.time)

  end
end
treeview_main.signal_connect("row-activated") do |treeview, path, column|
  model = treeview.model
  iter = model.get_iter(path)
  Launchy.open(iter.get_value(1))
end

treeview_main.signal_connect("size-allocate") {
  if @at_top
    @scrolled_main.vadjustment.value = 0
  end
}

@items.each do |i|
  iter = @model.prepend(nil)
  iter.set_value(0, "<span foreground=\"blue\" underline=\"single\">#{i[:title]}</span> <span foreground=\"grey\">#{i[:timestamp].strftime("%d/%m/%Y")}</span>")
  iter.set_value(1, i[:url])
  iter.set_value(2, i[:id])
  iter.set_value(3, i[:title])
  #iter.set_value(3, "<span align=\"right\" foreground=\"grey\">#{i[:timestamp].strftime("%m/%d/%Y")}</span>")
end

# Entry form
@context = @textentry_main.style_context

@revealer.set_transition_duration(500)
@revealer.set_transition_type(Gtk::Revealer::TransitionType::SLIDE_LEFT)

search_button.signal_connect("clicked") {
  add_url
}

@textentry_main.signal_connect("notify::text") {
  @context.remove_class(Gtk::STYLE_CLASS_ERROR)
}

@textentry_main.signal_connect("insert-text") {
  puts "insert-text"
  @revealer.set_reveal_child(true)
}

@textentry_main.signal_connect_after("cut-clipboard") {
  puts "cut-clipboard"
  if @textentry_main.text.size < 1
    @revealer.set_reveal_child(false)
  end
}

@textentry_main.signal_connect_after("delete-from-cursor") do |textentry, delete_type, position|
  puts "delete-from-cursor"
  if @textentry_main.text.size < 1
    @revealer.set_reveal_child(false)
  end
end

@textentry_main.signal_connect_after("backspace") {
  puts "backspace"
  puts @textentry_main.text.size
  if @textentry_main.text.size < 1
    @revealer.set_reveal_child(false)
  end
}

@textentry_main.signal_connect("activate") {
  add_url
}

def add_url
  begin
    uri = URI.parse(@textentry_main.text.strip)
    if uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS)
      begin
        website = PublicSuffix.parse(uri.host)
        puts website.domain
        doc = Nokogiri::HTML(open(uri), nil, Encoding::UTF_8.to_s)

        if website.domain == "youtube.com"
          name = doc.at_css('[id="eow-title"]').inner_html.strip.to_s
        elsif website.domain == "youtu.be"
          name = doc.at_css('[id="eow-title"]').inner_html.strip.to_s
        elsif website.domain == "bandcamp.com"
          name_section = doc.at_css('[id="name-section"]')
          title = name_section.at('h2').inner_html.strip.to_s
          artist =  name_section.at('a').inner_html.strip.to_s
          name = "#{title} by #{artist}"
        elsif website.domain == "soundcloud.com"
          name_section = doc.at('title').inner_html.strip.to_s
          name =  name_section.split('|')[0]
        end

        if @scrolled_main.vadjustment.value == 0
          @at_top = true
        else
          @at_top = false
        end

        @textentry_main.set_text("")
        @revealer.set_reveal_child(false)
        begin
          ts = Time.now
          id = @items.insert(:title => name, :url => uri.to_s, :timestamp => ts)
          iter = @model.prepend(nil)
          iter.set_value(0, "<span foreground=\"blue\" underline=\"single\">#{name}</span> <span foreground=\"grey\">#{ts.strftime("%d/%m/%Y")}</span>")
          iter.set_value(1, uri.to_s)
          iter.set_value(2, id)

        rescue Sequel::UniqueConstraintViolation => e
          #case e
            #when
              puts "i am rescued"
              puts e.class
              @status_revealer.set_reveal_child(true)
              GLib::Timeout.add_seconds(2) {
                @status_revealer.set_reveal_child(false)
                false
              }
          #end
        end


      rescue => e
        case e
          when OpenURI::HTTPError
            # handle
            puts "OpenURI::HTTPError"
          when SocketError
            # handle
            puts "SocketError"
          else
            raise e
        end
      rescue SystemCallError => e
        if e === Errno::ECONNRESET
          # handle
          puts "Errno::ECONNRESET"
        else
          raise e
        end
      end
    else
      puts "Bad URI"
      @context.add_class(Gtk::STYLE_CLASS_ERROR)
    end
  rescue => e
    puts "Really bad URI"
    @context.add_class(Gtk::STYLE_CLASS_ERROR)
  end
end

app_icon = GdkPixbuf::Pixbuf.new(:file => "icon.svg")

statusicon = Gtk::StatusIcon.new
statusicon_menu = Gtk::Menu.new

exititem_menu = Gtk::MenuItem.new(:label => "Exit")
exititem_menu.signal_connect("activate") {
  exit_app
}
statusicon_menu.append(exititem_menu)
statusicon.pixbuf = app_icon
statusicon.signal_connect("popup-menu")  do |w, button, activate_time|
  statusicon_menu.show_all
  statusicon_menu.popup(nil, nil, button, activate_time)
  puts "popup-menu"
end
@position = [0,0]
statusicon.signal_connect("activate") {
  puts "activate"
  if @window_main.visible?
    @position = @window_main.position
    @window_main.hide
  else
    @window_main.show
    @window_main.move(@position[0], @position[1])
  end
  #window_main.iconify
}

def exit_app
  @keyfile.set_double_list("window_group", "position", @window_main.position)
  @keyfile.set_double_list("window_group", "size", @window_main.size)
  puts "Saving settings to KeyFile."
  File.open("#{@cd}/#{@keyfilename}", "w") do |out|
    out.write @keyfile.to_data
  end
  puts "Exiting."
  Gtk.main_quit
end

Signal.trap("INT") {
  exit_app
}
Signal.trap("TERM") {
  exit_app
}

@window_main.icon = app_icon
@window_main.signal_connect("delete-event") {
  @position = @window_main.position
  @window_main.hide_on_delete
#  Gtk.main_quit
}
begin
  window_pos = @keyfile.get_integer_list("window_group", "position")
  window_size = @keyfile.get_integer_list("window_group", "size")
  @window_main.move(window_pos[0], window_pos[1])
  @window_main.resize(window_size[0], window_size[1])
rescue => e
  puts e
end
@window_main.show
Gtk.main
